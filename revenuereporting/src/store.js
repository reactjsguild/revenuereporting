import { createStore, applyMiddleware } from "redux";
import { customMiddleWare } from "./middleware/middleware";
import { composeWithDevTools } from "redux-devtools-extension";
import reducer from "./reducers/captureexcel";

const store = createStore(
  reducer,
  null,
  composeWithDevTools(applyMiddleware(customMiddleWare))
);
///const store = createStore(reducer);
export default store;
