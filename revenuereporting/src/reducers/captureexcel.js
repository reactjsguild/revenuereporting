const initialState = {
  data: {},
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "CAPTUREEXCEL":
      return {
        ...state,
        data: payload,
      };
    default:
      return { ...state };
  }
};
export default reducer;
