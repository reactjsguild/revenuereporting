import React, { PureComponent } from "react";
import "./displaydata.css";

class DisplayData extends PureComponent {
  render() {
    const { data } = this.props;
    if (!data.rows) return "";
    const { rows, cols } = data;
    return (
      <div>
        {cols.length &&
          cols.map(item => <span className="headercolumn">Row</span>)}
      </div>
    );
  }
}
export default DisplayData;
