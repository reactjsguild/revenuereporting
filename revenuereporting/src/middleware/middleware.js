export const customMiddleWare = (store) => (next) => (action) => {
  console.log("Triggered an action", action.type);
  console.log("current State", store.getState());
  next(action);
};
