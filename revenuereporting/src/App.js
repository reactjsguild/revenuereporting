import React from "react";
import { OutTable, ExcelRenderer } from "react-excel-renderer";
import DisplayData from "./components/DisplayData";
import { connect } from "react-redux";
import { readExcel } from "./actions/ReadExcel";
import "./App.css";

class App extends React.PureComponent {
  constructor(props) {
    super(props);
  }
  readExcelnValidate = () => {
    console.log("Validation will happen here");
  };
  handleSelection = () => event => {
    let excelFiLe = event.target.files[0];
    ExcelRenderer(excelFiLe, (err, resp) => {
      if (err) {
        console.log("error occured while reading the file", excelFiLe);
      } else {
        console.log("ACtion triggered");
        this.props.setData(resp);
      }
    });
  };
  render() {
    const { data } = this.props;
    return (
      <div>
        <h2>Upload the excel file to process:</h2>
        <input type="file" onChange={this.handleSelection(this)} />
        {data && <DisplayData data={data} />}
        <button onClick={this.readExcelnValidate}> Validate </button>
      </div>
    );
  }
}

const mapStatetoProps = state => ({
  data: state.data
});

const mapDispatchtoProps = dispatch => ({
  setData: value => dispatch(readExcel(value))
});

export default connect(mapStatetoProps, mapDispatchtoProps)(App);
